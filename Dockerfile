FROM ubuntu:19.10

ENV MINIMAP2_VERSION 2.17
ENV NANOPOLISH_VERSION 0.12.3
ENV SAMTOOLS_VERSION 1.10
ENV HTSLIB_VERSION 1.10.2
ENV NANOPOLISHCOMP_VERSION 0.6.11

MAINTAINER Mattia Furlan <mattia.furlan@iit.it>

RUN export DEBIAN_FRONTEND=noninteractive

RUN apt-get update

RUN apt-get -y install build-essential

RUN apt-get -y install git

RUN apt-get -y install -y wget

RUN apt-get -y install libz-dev

### Install Minimap2
RUN wget -O /minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 "https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2" \
&& tar -jxf /minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 \
&& cp /minimap2-${MINIMAP2_VERSION}_x64-linux/minimap2 /usr/local/bin

### HTSlib and Samtools
RUN git clone https://github.com/samtools/htslib.git \
&& cd /htslib \
&& git checkout ${HTSLIB_VERSION}

### Install nanopolish
RUN git clone --recursive https://github.com/jts/nanopolish.git \
&& cd /nanopolish \
&& git checkout "v${NANOPOLISH_VERSION}" \
&& make && cp /nanopolish/nanopolish /usr/local/bin

RUN apt-get -y install autoconf
RUN apt-get -y install libbz2-dev
RUN apt-get -y install liblzma-dev
RUN apt-get -y install libcurl4-openssl-dev

### HTSlib and Samtools
RUN git clone https://github.com/samtools/samtools.git \
&& cd /samtools \
&& git checkout ${SAMTOOLS_VERSION} \
&& autoreconf \
&& ./configure --without-curses --with-htslib=../htslib \
&& make \
&& cp samtools /usr/local/bin

### Python3 and nanopolishcomp
RUN apt-get -y install python3
RUN apt-get -y install python3-pip
RUN apt-get -y install python3-tk
RUN apt-get -y install python3-numpy
RUN apt-get -y install python3-scipy

RUN pip3 install nanopolishcomp=="${NANOPOLISHCOMP_VERSION}"
